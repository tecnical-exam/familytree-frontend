import { useState, useEffect} from 'react';
import { Card, Button, Form, Modal } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';


export default function MemberView() {

	const navigate = useNavigate();

	const { nameId } = useParams();
	
	const [name, setName] = useState("");
	const [address, setAddress] = useState("");
	const [contactNo, setContactNo] = useState("");
	const [birthday, setBirthday] = useState("");

	const [showEdit, setShowEdit] = useState(false);


	const openEdit = () => {

		setShowEdit(true)
	}

	const closeEdit = () => {
		
		setShowEdit(false)
	}

	const editName = (e) => {
		e.preventDefault()

		fetch(`https://shrouded-inlet-31679.herokuapp.com/api/users/updateData/${nameId}`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				name: name,
				address: address,
				contactNo: contactNo,
				birthday: birthday
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Item successfully updated"
				})
				closeEdit()

			}else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})

					
			}
		})
	}

	const archiveMember = (nameId) => {
		fetch(`https://shrouded-inlet-31679.herokuapp.com/api/users/archiveName/${nameId}`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data => {
			//console.log(data)

			if (data){
				Swal.fire({
					title: 'Data Deleted',
					icon: "success",
					text: "Family Member Removed"
				})
				navigate("/")	

			} else {
				Swal.fire({
					title: 'Something went wrong',
					icon: "error",
					text: "Please try again"	
				})
			}
		})
	}

	useEffect(() => {
		//console.log(itemId)
		fetch(`https://shrouded-inlet-31679.herokuapp.com/api/users/${nameId}`)
		.then(res => res.json())
		.then(data => {
			//console.log(data)

			setName(data.name)
			setAddress(data.address)
			setContactNo(data.contactNo)
			setBirthday(data.birthday)
		})

	}, [nameId])

	return(
		<>
			<Card className = "shadow md={4}"style={{width: '18rem'}} >
				  <Card.Img variant="top" src="https://media.istockphoto.com/vectors/user-member-vector-icon-for-ui-user-interface-or-profile-face-avatar-vector-id1130884625?k=20&m=1130884625&s=612x612&w=0&h=OITK5Otm_lRj7Cx8mBhm7NtLTEHvp6v3XnZFLZmuB9o=	" />
				  <Card.Body>
				    <Card.Title>{name}</Card.Title>
					<Card.Title>{address}</Card.Title>
					<Card.Title>{contactNo}</Card.Title>
					<Card.Text>{birthday}</Card.Text>
						<Button className="mx-2" variant="outline-primary" as={Link} to={`/`}>Back</Button>
						<Button className="mx-2" variant="outline-warning" onClick={openEdit}>Edit</Button>
					    <Button className="mx-2" variant="outline-danger"onClick={() => archiveMember(nameId)}>Delete</Button>
				    
				  </Card.Body>
			</Card>

			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={e => editName(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Update Data</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group controlId="">
							<Form.Label>Name</Form.Label>
							<Form.Control
								value={name}
								onChange={e => setName(e.target.value)}
								type="text"
								required
							/>
						</Form.Group>

						<Form.Group controlId="">
							<Form.Label>Description</Form.Label>
							<Form.Control
								value={address}
								onChange={e => setAddress(e.target.value)}
								type="text"
								required
							/>
						</Form.Group>

						<Form.Group controlId="">
							<Form.Label>Contact No.</Form.Label>
							<Form.Control
								value={contactNo}
								onChange={e => setContactNo(e.target.value)}
								type="text"
								required
							/>
						</Form.Group>

						<Form.Group controlId="">
							<Form.Label>Birthday</Form.Label>
							<Form.Control
								value={birthday}
								onChange={e => setBirthday(e.target.value)}
								type="date"
							/>
						</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit}>Cancel</Button>
						<Button variant="success" type="submit">Save</Button>
					</Modal.Footer>
				</Form>
			</Modal>
		</>
	)
}

