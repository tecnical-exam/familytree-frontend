import {useState, useEffect} from 'react';
import {Row, Col,Button, Form, Modal } from 'react-bootstrap';
import NameView from  "../components/NameView"
import Swal from 'sweetalert2';

export default function Home(){

	
	const [nameData, setNameData] = useState([])

	const [name, setName] = useState("")
	const [address, setAddress] = useState("")
	const [contactNo, setContactNo] = useState("")
	const [birthday, setBirthday] = useState("")

	const [showAdd, setShowAdd] = useState(false)

	const openAdd = () => setShowAdd(true)
	const closeAdd = () => setShowAdd(false)

	const fetchData = () => {
		fetch(`https://shrouded-inlet-31679.herokuapp.com/api/users/allNames`)
		.then(res => res.json())
		.then(data => {
			setNameData(data)
		
		})
	}

	useEffect(() => {
			fetchData()
		}, [])

	const addMember = (e) => {
		e.preventDefault()
		fetch(`https://shrouded-inlet-31679.herokuapp.com/api/users/saveUser`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"},
			body: JSON.stringify({
				name: name,
				address: address,
				contactNo: contactNo,
				birthday: birthday
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Family Member successfully added"
				})

				fetchData()
				closeAdd()

				setName("")
				setAddress("")
				setContactNo("")
				setBirthday("")
			}else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})

				fetchData()		
			}
		})
	}


	return(
			<Row>
				<Col className="my-5">
					<h3>Welcome!</h3>
					<p>You can add, update and remove family members</p>
					<Button variant="primary" onClick={openAdd}>Add a Family Member</Button>
				</Col>
				<Row>
					<NameView nameProp={nameData}/>
				</Row>

				<Modal show={showAdd} onHide={closeAdd}>
					<Form onSubmit={e => addMember(e)}>
						<Modal.Header closeButton>
							<Modal.Title>Add a Family Member</Modal.Title>
						</Modal.Header>

						<Modal.Body>
							<Form.Group controlId="memberName">
								<Form.Label>Name</Form.Label>
								<Form.Control
									value={name}
									onChange={e => setName(e.target.value)}
									type="text"
									required
								/>
							</Form.Group>

							<Form.Group controlId="memberAddress">
								<Form.Label>Address</Form.Label>
								<Form.Control
									value={address}
									onChange={e => setAddress(e.target.value)}
									type="text"
									required
								/>
							</Form.Group>

							<Form.Group controlId="memberContactNo">
								<Form.Label>Contact No.</Form.Label>
								<Form.Control
									value={contactNo}
									onChange={e => setContactNo(e.target.value)}
									type="text"
									required
								/>
							</Form.Group>

							<Form.Group controlId="memberBirthday">
								<Form.Label>Birthday</Form.Label>
								<Form.Control
									value={birthday}
									onChange={e => setBirthday(e.target.value)}
									type="date"
									required
								/>
							</Form.Group>
						</Modal.Body>

						<Modal.Footer>
							<Button variant="secondary" onClick={closeAdd}>Cancel</Button>
							<Button variant="success" type="submit">Save</Button>
						</Modal.Footer>
					</Form>
				</Modal>	
			</Row>
		)
}