//import {Fragment, useContext} from 'react';
import {Navbar, Container} from 'react-bootstrap';
import {Link} from 'react-router-dom';


export default function AppNavbar(){

	return(
			<Navbar className = "mb-5" bg="light" expang="lg">
				<Container fluid>	
					<Navbar.Brand as = {Link} to ="/"> Family Tree App | </Navbar.Brand>
				</Container>
			</Navbar>	

		)
}
