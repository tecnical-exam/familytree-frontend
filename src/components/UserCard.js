
import {Card, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';


export default function UserCard({namesProp}) {

	
	const { name, _id} = namesProp
	// console.log(namesProp)
	return(
		<>
			<Card className = "d-inline-flex m-2 shadow md={4}"style={{width: '18rem'}} >
			  <Card.Img variant="top" src="https://media.istockphoto.com/vectors/user-member-vector-icon-for-ui-user-interface-or-profile-face-avatar-vector-id1130884625?k=20&m=1130884625&s=612x612&w=0&h=OITK5Otm_lRj7Cx8mBhm7NtLTEHvp6v3XnZFLZmuB9o=	" />
			  <Card.Body>
			    <Card.Title >{name}</Card.Title>
			    <Button variant="outline-dark" as={Link} to={`/${_id}`}>view details</Button>
			  </Card.Body>
			</Card>

			
		</>
		)

}





