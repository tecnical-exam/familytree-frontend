import { useState, useEffect } from 'react';

import UserCard from './UserCard'

export default function NameView({nameProp}) {

	const [nameArr, setNameArr] = useState([])

	useEffect(() => {
		const names = nameProp.map(name => {
			if(name.isActive){
				return <UserCard key={name._id} namesProp={name} />
			}else{
				return null
			}
		})

		setNameArr(names)

	}, [nameProp])

	return(
		<>
			{nameArr}
		</>
	)
}
