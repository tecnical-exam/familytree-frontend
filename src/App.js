
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import {Container} from 'react-bootstrap';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import ErrorPage from './pages/ErrorPage'
import MemberView from './pages/MemberView'

import './App.css';

function App() {
  return (
    <Router>
      <AppNavbar />
      <Container>
        <Routes>
          <Route path ="/" element={<Home />} />
          <Route path="/:nameId" element={<MemberView />} />
          <Route path ="*" element={<ErrorPage />} />
        </Routes>
       </Container>
    </Router>
  );
}

export default App;
